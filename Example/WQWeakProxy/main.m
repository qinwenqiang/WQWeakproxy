//
//  main.m
//  WQWeakProxy
//
//  Created by qinwenqiang on 05/18/2021.
//  Copyright (c) 2021 qinwenqiang. All rights reserved.
//

@import UIKit;
#import "WQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WQAppDelegate class]));
    }
}
