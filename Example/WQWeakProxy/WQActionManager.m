//
//  KDActionManager.m
//  RacDemo
//
//  Created by 秦文强 on 2021/5/14.
//

#import "WQActionManager.h"
#import <UIKit/UIKit.h>
#import <WQMutiDelegateProxy.h>

@implementation WQActionManager
+ (instancetype)shareInstance{
    static WQActionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [WQActionManager new];
    });
    return manager;
}
- (WQMutiDelegateProxy *)delegateProxy{
    if (!_delegateProxy) {
        _delegateProxy = (id<WQActionManagerProtocol>)[WQMutiDelegateProxy alloc];
    }
    return _delegateProxy;
}

- (void)addMutiDelegate:(id<WQActionManagerProtocol>)object{
    [self.delegateProxy addMutiDelegate:object];
}
- (void)receiveAction{
    if ([self.delegateProxy respondsToSelector:@selector(testMethordAllResponse:)]) {
        [self.delegateProxy testMethordAllResponse:[UIColor colorWithRed:(arc4random()%255)/255.0 green:(arc4random()%255)/255.0 blue:(arc4random()%255)/255.0 alpha:1.0]];
    }
    if ([self.delegateProxy respondsToSelector:@selector(testMethordPartResponse)]) {
        [self.delegateProxy testMethordPartResponse];
    }
    if ([self.delegateProxy respondsToSelector:@selector(testMethordWithReturnValue)]) {
        NSLog(@"%@",[self.delegateProxy testMethordWithReturnValue]);
    }
    if ([self.delegateProxy respondsToSelector:@selector(testMethordNotResponse)]) {
        NSLog(@"error: 响应了不应该响应的方法");
    }
    
}
@end
