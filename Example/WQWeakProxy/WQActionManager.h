//
//  WQActionManager.h
//  RacDemo
//
//  Created by 秦文强 on 2021/5/14.
//

#import <Foundation/Foundation.h>
@class WQMutiDelegateProxy;
@protocol WQActionManagerProtocol <NSObject>
@optional
- (NSString *)testMethordWithReturnValue;
- (void)testMethordPartResponse;
- (void)testMethordAllResponse:(UIColor *)color;
- (void)testMethordNotResponse;
@end

@interface WQActionManager : NSObject
+ (instancetype)shareInstance;
///代理 分发器
@property (nonatomic, strong) WQMutiDelegateProxy<WQActionManagerProtocol> *delegateProxy;

/// 添加代理
/// @param object 代理
- (void)addMutiDelegate:(id<WQActionManagerProtocol>)object;

/// 模拟收到了事件
- (void)receiveAction;
@end

