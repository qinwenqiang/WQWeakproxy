//
//  WQMutiDelegateProxy.m
//  RacDemo
//
//  Created by 秦文强 on 2021/5/13.
//

#import "WQMutiDelegateProxy.h"
@interface WQMutiDelegateProxy ()
///代理数组
@property (nonatomic, strong) NSPointerArray *delegates;
@end

@implementation WQMutiDelegateProxy
- (NSPointerArray *)delegates{
    if (!_delegates) {
        _delegates = [NSPointerArray weakObjectsPointerArray];
    }
    return _delegates;
}


- (void)clearNull{
    /**
      compact 函数有个已经报备的 bug: compact 调用之后，NULL 并未被剔除
      出现这种 bug 的原因和 compact 函数的实现机制有关，当我们主动给 NSPointerArray 添加 NULL 时，数组会标记有空元素插入，此时 compact 函数才会生效，也就是说，compact 函数会先判断是否有标记，之后才会剔除。所以，当直接 set count，或者成员已经释放，自动置空的情况出现时，就会出现这个 bug。
      解决也很简单：
      在调用 compact 之前，手动添加一个 NULL，触发标记
     */
    if (self.delegates.count != self.delegates.allObjects.count) {
        [self.delegates addPointer:NULL];
        [self.delegates compact];
    }
}
#pragma mark - methord
- (void)addMutiDelegate:(id)object{
    // 删除数组中的nil值
    [self clearNull];
    
    if ([self.delegates.allObjects containsObject:object] || object == nil || [object isKindOfClass:[NSNull class]]) {
        return;
    }
    [self.delegates addPointer:(__bridge void * _Nullable)(object)];

}
- (NSArray *)allDelegates{
    [self clearNull];
    return self.delegates.allObjects;
}

- (void)removeDelegate:(id)object{
    [self clearNull];
    NSInteger index = [self.delegates.allObjects indexOfObject:object];
    if (index != NSNotFound) {
        [self.delegates removePointerAtIndex:index];
    }
    
    
}


#pragma mark - 消息转发

- (BOOL)respondsToSelector:(SEL)aSelector{
    BOOL responds = NO;
    for (id delegate in self.delegates.allObjects) {
        if ([delegate respondsToSelector:aSelector]) {
            responds =  YES;
            break;
        }
    }
    return responds;
}

/**
  Normal forwarding 的第一步，也是消息转发的最后一次机会--这个针对NSObject, 对于 NSProxy 未实现立马走这里
  消息获得函数的参数和返回值类型，即返回一个函数签名
  返回携带参数类型、返回值类型和长度等的 selector 签名信息 NSMethodSignature对象，Runtime 内部会基于 NSMethodSignature 实例构建一个NSInvocation 对象，作为回调- (void)forwardInvocation:的入参
 
  @param sel selector 方法选择子
  @return NSMethodSignature 函数签名
         返回nil，Runtime 则会发出 -doesNotRecognizeSelector: 消息，程序 crash
         返回了NSMethodSignature，Runtime 就会创建一个 NSInvocation 对象并发送 -forwardInvocation: 消息给目标对象
 */
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel{
    //如果没有任何代理 随便抛出一个签名 防止崩溃
    if (self.delegates.allObjects.count == 0) {
        return [NSObject instanceMethodSignatureForSelector:@selector(init)];
    }
    for (id delegate in self.delegates.allObjects) {
        if ([delegate respondsToSelector:sel]) {
            return [delegate methodSignatureForSelector:sel];
        }
    }
    [self showErrorSel:sel];
    return nil;
}

/**
 可以在 -forwardInvocation: 里修改传进来的 NSInvocation 对象，然后发送 -invokeWithTarget: 消息给它，传进去新的目标执行

 @param invocation 对一个消息的描述，包括 selector 以及参数等信息
 */
- (void)forwardInvocation:(NSInvocation *)invocation{
    
    if (self.delegates.allObjects.count == 0) {
        //因为前面抛出的是init方法签名 会返回一个实例 修改为返回nil
        void *null = NULL;
        [invocation setReturnValue:&null];
        return;
    }
    BOOL invoked = NO;
    for (id delegate in self.delegates.allObjects) {
        if ([delegate respondsToSelector:invocation.selector]) {
            [invocation invokeWithTarget:delegate];
            invoked = YES;
        }
    }
    if (!invoked) {
        [self showErrorSel:invocation.selector];
    }
}
- (void)showErrorSel:(SEL)selector{
    NSString *error = [NSString stringWithFormat:@"\nerror:\n%s\n\"%@\" 代理方法当前没有任何代理可以实现\n delegates:%@",__func__,NSStringFromSelector(selector),self.delegates.allObjects];
    NSAssert(0, error);
}
- (void)dealloc{
    NSLog(@"%s",__func__);
}


@end



/**
   NSProxy 与 NSObject 的消息传递的不同
   NSObject 收到消息会先去缓存列表查找SEL，若是找不到，就到自身方法列表中查找，依然找不到就顺着继承链进行查找，依然找不到的话，那就是 unknown selector，进入消息转发程序
   1. +(BOOL)resolveInstanceMethod: 其返回值为Boolean类型，表示这个类是否通过class_addMethod新增一个实例方法用以处理该 unknown selector，也就是说在这里可以新增一个处理 unknown selector的方法，若不能，则继续往下传递（若 unknown selector是类方法，那么调用 +(BOOL)resolveClassMethod:）
   2. - (id)forwardingTargetForSelector: 这是第二次机会进行处理 unknown selector，即转移给一个能处理 unknown selector的其它对象，若返回一个其它的执行对象，那消息从 id objc_msgSend ( id self, SEL op, ...) 重新开始，若不能，则返回 nil，并继续向下传递，最后的一次消息处理机会（3 与 4 配套）
   3. - (NSMethodSignature *)methodSignatureForSelector: 返回携带参数类型、返回值类型和长度等的 selector 签名信息 NSMethodSignature对象，Runtime 内部会基于 NSMethodSignature 实例构建一个NSInvocation 对象，作为回调- (void)forwardInvocation:的入参
   4. - (void)forwardInvocation:这一步可以对传进来的 NSInvocation 进行一些操作，它主要在对象之间或者应用程序之间存储和转发消息（命令模式的实现），灵活性很高，譬如修改 target 、参数、甚至返回值，
   对于 NSProxy 就没有这么复杂了，接收到 unknown selector 后，直接回调- (NSMethodSignature *)methodSignatureForSelector: 和 - (void)forwardInvocation:, 消息转发过程简单的很多
 */
