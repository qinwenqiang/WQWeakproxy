//
//  SubViewController.m
//  NSProxyDemo
//
//  Created by 秦文强 on 2021/5/14.
//

#import "SubViewController.h"
#import "WQActionManager.h"
#import <WQWeakProxy.h>

@interface SubViewController ()<WQActionManagerProtocol>
@end

@implementation SubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor brownColor];
    
    [[WQActionManager shareInstance] addMutiDelegate:self];
    
    testView *test = [[testView alloc] initWithFrame:CGRectMake(0, 0, 150, 50)];
    test.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    test.tag = 999;
    [self.view addSubview:test];

    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 25, 120, 50, 20)];
    [btn setTitle:@"执行" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
}

- (void)btnAction{
    [[WQActionManager shareInstance] receiveAction];;
}

#pragma mark - KDActionManagerDelegate
- (NSString *)testMethordWithReturnValue{
    NSLog(@"%s",__func__);
    return [NSStringFromClass(self.class) stringByAppendingString:@"响应了返回值方法"];
}
- (void)testMethordAllResponse:(UIColor *)color{
    NSLog(@"%s",__func__);
    self.view.backgroundColor = color;
}

- (void)dealloc{
    NSLog(@"%s 多代理分发器没有造成循环引用",__func__);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    //hook掉presentingVC的touchesBegan事件
}

@end




#pragma mark - testView

@interface testView ()<WQActionManagerProtocol>
///计时器
@property (nonatomic, strong) NSTimer *timer;
///laber
@property (nonatomic, strong) UILabel *laber;
@end

@implementation testView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
        [[WQActionManager shareInstance] addMutiDelegate:self];

    }
    return self;
}

- (void)setUI{
    self.backgroundColor = [UIColor orangeColor];
    self.laber = [[UILabel alloc] initWithFrame:self.bounds];
    self.laber.textColor = [UIColor whiteColor];
    self.laber.font = [UIFont boldSystemFontOfSize:20];
    self.laber.adjustsFontSizeToFitWidth = YES;
    self.laber.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.laber];
}

- (void)timeAction{
    NSInteger second = self.laber.text.integerValue;
    self.laber.text = [NSString stringWithFormat:@"%td",++second];
}

#pragma mark - KDActionManagerDelegate
- (void)testMethordAllResponse:(UIColor *)color{
    NSLog(@"%s",__func__);
    self.laber.textColor = color;
}
- (void)testMethordPartResponse{
    NSLog(@"%s",__func__);
    if (self.timer) {
        return;
    }
    self.laber.text = @"0";
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:weak_Proxy(self) selector:@selector(timeAction) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}
- (void)dealloc{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    NSLog(@"%s timer没有造成循环引用",__func__);
}


@end
