//
//  WQMutiDelegateProxy.h
//  RacDemo
//
//  Created by 秦文强 on 2021/5/13.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface WQMutiDelegateProxy : NSProxy
/// 添加代理
/// @param object 代理
- (void)addMutiDelegate:(id)object;

/// 删除代理
/// @param object 代理
- (void)removeDelegate:(id)object;

/// 获取当前所有代理
- (NSArray *)allDelegates;
@end

