//
//  WQAppDelegate.h
//  WQWeakProxy
//
//  Created by qinwenqiang on 05/18/2021.
//  Copyright (c) 2021 qinwenqiang. All rights reserved.
//

@import UIKit;

@interface WQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
