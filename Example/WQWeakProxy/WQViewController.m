//
//  WQViewController.m
//  WQWeakProxy
//
//  Created by qinwenqiang on 05/18/2021.
//  Copyright (c) 2021 qinwenqiang. All rights reserved.
//

#import "WQViewController.h"
#import "WQActionManager.h"
#import "SubViewController.h"
#import <WQMutiDelegateProxy.h>
@interface WQViewController ()<WQActionManagerProtocol>

@end

@implementation WQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blueColor];

    [[WQActionManager shareInstance] addMutiDelegate:self];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 25, 120, 50, 20)];
    [btn setTitle:@"执行" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 222;
    [self.view addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 50, 180, 100, 20)];
    [btn setTitle:@"添加代理" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = 333;
    [self.view addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 50, 240, 100, 20)];
    [btn setTitle:@"移除代理" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}
- (void)btnAction:(UIButton *)sender{
    if (sender.tag == 222) {
        [[WQActionManager shareInstance] receiveAction];
    } else if (sender.tag == 333){
        SubViewController *subVc = [SubViewController new];
        [self presentViewController:subVc animated:YES completion:nil];
    } else {
        [[WQActionManager shareInstance].delegateProxy removeDelegate:self];
    }
}

#pragma mark - KDActionManagerDelegate

- (void)testMethordAllResponse:(UIColor *)color{
    NSLog(@"%s",__func__);
    self.view.backgroundColor = color;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"%@",[[WQActionManager shareInstance].delegateProxy allDelegates]);
}


@end
