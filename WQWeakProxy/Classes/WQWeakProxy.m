//
//  WQWeakProxy.m
//  NSProxyDemo
//
//  Created by 秦文强 on 2021/5/14.
//

#import "WQWeakProxy.h"

@interface WQWeakProxy ()
///target
@property (nonatomic, weak) id target;
@end

@implementation WQWeakProxy

- (instancetype)initWithTarget:(id)target{
    _target = target;
    return self;
}
+ (instancetype)weakProxyWithTarget:(id)target{
    return [[WQWeakProxy alloc] initWithTarget:target];
}
#pragma mark - 消息转发
/**
  快速转发
  若返回一个其它的执行对象，那消息从 id objc_msgSend ( id self, SEL op, ...) 重新开始，若不能，则返回 nil，并继续向下传递
  弱引用对象已经被释放 此处返回的就是nil 要继续向下转发
 */
- (id)forwardingTargetForSelector:(SEL)aSelector{
    return _target;
}
/**
  方法签名
  只有引用对象为nil时才会走到这里
  此处返回了一个init的方法签名，防止崩溃
 */
- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel{
    return [NSObject instanceMethodSignatureForSelector:@selector(init)];
}
/**
  方法重定向
  只有调用了methodSignatureForSelector方法 并且返回值不为空时才会到这
  将上面返回的init方法签名的返回值改为nil
 */
- (void)forwardInvocation:(NSInvocation *)invocation{
    void *null = NULL;
    [invocation setReturnValue:&null];
}
- (void)dealloc{
    NSLog(@"%s",__func__);
}

@end

WQWeakProxy *weak_Proxy(id target){
    return [WQWeakProxy weakProxyWithTarget:target];
}
