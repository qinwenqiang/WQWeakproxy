//
//  WQWeakProxy.h
//  NSProxyDemo
//
//  Created by 秦文强 on 2021/5/14.
//

#import <Foundation/Foundation.h>

@interface WQWeakProxy : NSProxy

/// 实例方法初始化
/// @param target 弱引用的对象
- (instancetype)initWithTarget:(id)target;

/// 类方法初始化
/// @param target 弱引用的对象
+ (instancetype)weakProxyWithTarget:(id)target;
@end

/// 快速初始化一个弱引用桥接
/// @param target 弱引用的对象
WQWeakProxy *weak_Proxy(id target);



