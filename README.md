# WQWeakProxy

[![CI Status](https://img.shields.io/travis/qinwenqiang/WQWeakProxy.svg?style=flat)](https://travis-ci.org/qinwenqiang/WQWeakProxy)
[![Version](https://img.shields.io/cocoapods/v/WQWeakProxy.svg?style=flat)](https://cocoapods.org/pods/WQWeakProxy)
[![License](https://img.shields.io/cocoapods/l/WQWeakProxy.svg?style=flat)](https://cocoapods.org/pods/WQWeakProxy)
[![Platform](https://img.shields.io/cocoapods/p/WQWeakProxy.svg?style=flat)](https://cocoapods.org/pods/WQWeakProxy)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WQWeakProxy is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WQWeakProxy'
```

## Author

qinwenqiang, qinwenqiang@kding.com

## License

WQWeakProxy is available under the MIT license. See the LICENSE file for more info.
